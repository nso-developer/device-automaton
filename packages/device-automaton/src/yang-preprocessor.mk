include $(NCS_DIR)/src/ncs/build/vsn.mk

NSO_VERSION_MAJOR=$(word 1,$(subst ., ,$(NCSVSN)))
NSO_VERSION_MINOR=$(word 2,$(subst _, ,$(subst ., ,$(NCSVSN))))

SRC += yang/device-automaton-groupings.yang
YANGPATH += $(DEVICE_AUTOMATON_PACKAGE)yang

yang/%.yang: $(DEVICE_AUTOMATON_PACKAGE)yang-pp/%.yang
	if [ ${NSO_VERSION_MAJOR} -eq 5 ] && [ ${NSO_VERSION_MINOR} -ge 6 ] || [ ${NSO_VERSION_MAJOR} -gt 5 ]; then \
		cp $< $@; \
	else \
		sed '/^\s\+\/\/#BEGIN_NSO_GE_56/,/^\s\+\/\/#END_NSO_GE_56/d' $< > $@; \
	fi
