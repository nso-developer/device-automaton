"""NSO Authgroup service

The authgroup service creates an NSO authgroup (in /devices/authgroups/group)
given the credentials. The authgroup only supports the "default-map" mapping,
meaning all NSO users will map to the same set of device credentials.
"""

import _ncs
import ncs
from ncs.application import Service


def decrypt_encrypted_string(node, value):
    """Decrypts an encrypted tailf:aes-cfb-128-encrypted-string type leaf

    :param node: any maagic Node
    :param value: the encrypted leaf value
    """
    # pylint: disable=protected-access
    if value is None:
        return None

    if isinstance(node._backend, ncs.maagic._TransactionBackend):
        node._backend.maapi.install_crypto_keys()
    elif isinstance(node._backend, ncs.maagic._MaapiBackend):
        node._backend.install_crypto_keys()
    else:
        raise ValueError("Unknown MaagicBackend for leaf")

    return _ncs.decrypt(value) #pylint: disable=no-member


class AuthgroupService(Service):
    """Service callbacks for the authgroup service"""

    @Service.create
    def cb_create(self, tctx, root, service, proplist):
        """The "create" service callback for the authgroup service"""
        self.log.info(f"Service authgroup {service.name} for username {service.username}")

        # The service model enforces XOR between different private key types and password
        if encrypted_private_key := service.private_key:
            private_key = decrypt_encrypted_string(root, encrypted_private_key)
        else:
            private_key = ''
        tvars = ncs.template.Variables()
        tvars.add('PRIVATE_KEY', private_key)
        template = ncs.template.Template(service)
        template.apply('devaut-authgroup-template', tvars)

        return proplist


class AuthgroupServiceApp(ncs.application.Application):
    """Main application component of the authgroup service"""

    def setup(self):
        self.register_service('devaut-authgroup-servicepoint', AuthgroupService)
