import unittest
from pathlib import Path
from typing import List

from lxml import etree

from device_automaton.ned_id import *


def _extract_ned_info(file: str) -> List[NedInfo]:
    neds: List[NedInfo] = []

    nsmap = {'ncs': 'http://tail-f.com/ns/ncs', 'ned': 'http://tail-f.com/ns/ncs-ned'}
    root = etree.parse(file)
    for et_ned_id in root.xpath('//ncs:devices/ncs:ned-ids/ncs:ned-id', namespaces=nsmap):
        ned_id = et_ned_id.find('ncs:id', namespaces=nsmap).text
        ned_info = NedInfo.from_cdb(ned_id)
        neds.append(ned_info)
    return neds


class TestDetectNedId(unittest.TestCase):
    def test_detect_load(self):
        self.assertNotEqual(self.neds, None)

    def test_family(self):
        # test data contains alu-sr-cli-8.1 and alu-sr-cli-8.2
        # we expect the newer version to be selected
        alu = find_ned_by_family(self.neds, Identity.from_cdb('alu-sr-cli:alu-sr-cli'))
        self.assertEqual(str(alu.identity), 'alu-sr-cli-8.2:alu-sr-cli-8.2') # type: ignore

    def test_netconf(self):
        ned_info = NedInfo.from_cdb('alu-sr-cli:alu-sr-cli')
        self.assertFalse(ned_info.netconf)

        ned_info = NedInfo.from_cdb('ned:netconf')
        self.assertTrue(ned_info.netconf)

        ned_info = NedInfo.from_cdb('juniper-junos-nc-4.5:juniper-junos-nc-4.5')
        self.assertTrue(ned_info.netconf)
        base = Identity.from_cdb('juniper-junos-nc:juniper-junos-nc')
        self.assertTrue(ned_info.identity.derived_from(base))

        ned_info = NedInfo.from_cdb('ned-ios-xr-yang-6.5.1-nc-6.5:ned-ios-xr-yang-6.5.1-nc-6.5')
        self.assertTrue(ned_info.netconf)
        self.assertEqual(str(ned_info.family), 'ned-ios-xr-yang-6.5.1-nc:ned-ios-xr-yang-6.5.1-nc')

        ned_info = NedInfo.from_cdb('ned-kea-yang-nc-1.0:ned-kea-yang-nc-1.0')
        self.assertTrue(ned_info.netconf)
        self.assertEqual(str(ned_info.family), 'ned-kea-yang-nc:ned-kea-yang-nc')

    def test_base(self):
        identity = Identity.from_cdb('ned:netconf')
        self.assertEqual(str(identity.base.pop()), 'ned:netconf-ned-id')

        identity = Identity.from_cdb('alu-sr-cli-8.14:alu-sr-cli-8.14')
        self.assertEqual(str(identity.base.pop()), 'alu-sr-cli:alu-sr-cli')

        identity = Identity.from_cdb('alu-sr-cli:alu-sr-cli')
        self.assertEqual(str(identity.base.pop()), 'ned:cli-ned-id')

        identity = Identity.from_cdb('juniper-junos-nc-4.5:juniper-junos-nc-4.5')
        self.assertEqual(str(identity.base.pop()), 'juniper-junos-nc:juniper-junos-nc')

    def setUp(self):
        cur_dir = Path(__file__).parent
        self.neds = _extract_ned_info(str(cur_dir / 'ned-ids.xml'))
