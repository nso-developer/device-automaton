import re
from decimal import Decimal
import unittest
from pathlib import Path
from typing import Dict, Set

from lxml import etree

from device_automaton.device_detect_ned_id import YangModule, score_devices_to_ned_ids, find_best_ned_id


def _extract_ned_ids(file: str) -> Dict[str, Set[YangModule]]:
    neds: Dict[str, Set[YangModule]] = {}

    nsmap = {'ncs': 'http://tail-f.com/ns/ncs', 'ned': 'http://tail-f.com/ns/ncs-ned'}
    root = etree.parse(file)
    for et_ned_id in root.xpath('//ncs:devices/ncs:ned-ids/ncs:ned-id', namespaces=nsmap):
        ned_id = et_ned_id.find('ncs:id', namespaces=nsmap).text
        neds[ned_id] = set()
        for et_module in et_ned_id.xpath('ncs:module', namespaces=nsmap):
            ns = et_module.find('ncs:namespace', namespaces=nsmap).text
            name = et_module.find('ncs:name', namespaces=nsmap).text
            et_revision = et_module.find('ncs:revision', namespaces=nsmap)
            revision = et_revision.text if et_revision is not None else None
            neds[ned_id].add(YangModule(ns, name, revision))
    return neds


def _extract_device_capabilities(file: str) -> Set[YangModule]:
    capas = set()

    nsmap = {'ncs': 'http://tail-f.com/ns/ncs'}
    root = etree.parse(file)
    for et_cap in root.xpath('//ncs:devices/ncs:device/ncs:capability', namespaces=nsmap):
        uri = et_cap.find('ncs:uri', namespaces=nsmap).text
        et_revision = et_cap.find('ncs:revision', namespaces=nsmap)
        revision = et_revision.text if et_revision is not None else None
        et_module = et_cap.find('ncs:module', namespaces=nsmap)
        module = et_module.text if et_module is not None else None
        capas.add(YangModule(uri, module, revision))
    return capas


class TestDetectNedId(unittest.TestCase):
    def test_detect_ned_id_match(self):
        self.maxDiff = None
        expected = {'vmx15': 'juniper-junos-nc-4.5:juniper-junos-nc-4.5',
                    'sros19': 'ned-sros-yang-nc-1.0:ned-sros-yang-nc-1.0',
                    'xr642': 'ned-ios-xr-yang-nc-6.4:ned-ios-xr-yang-nc-6.4',
                    # Surprisingly, XRv 6.4.2 has more matches for the
                    # advertised capabilities in the 6.5.1 NED than in 6.4.2?!
                    'xrv642': 'ned-ios-xr-yang-6.5.1-nc-6.5:ned-ios-xr-yang-6.5.1-nc-6.5',
                    'xrv651': 'ned-ios-xr-yang-6.5.1-nc-6.5:ned-ios-xr-yang-6.5.1-nc-6.5',
                    'xrv652': 'ned-ios-xr-yang-6.5.2-nc-6.5:ned-ios-xr-yang-6.5.2-nc-6.5',
                    'ned-junos-yang': 'juniper-junos-nc-4.5:juniper-junos-nc-4.5'}
        found = score_devices_to_ned_ids(self.devices, self.neds)
        self.assertEqual(expected, find_best_ned_id(found))

    def test_false_positive_match(self):
        self.neds.pop('juniper-junos-nc-4.5:juniper-junos-nc-4.5')
        devices = {'ned-junos-yang': self.devices['ned-junos-yang']}
        found = score_devices_to_ned_ids(devices, self.neds)
        best = find_best_ned_id(found)['ned-junos-yang']
        best_score = found['ned-junos-yang'][best] # type: ignore
        self.assertEqual('ned-tcx1000-yang-nc-1.0:ned-tcx1000-yang-nc-1.0', best)
        self.assertEqual(Decimal('0.05084745762711864406779661017'), best_score)

    def test_detect_ned_id_no_match(self):
        expected = {'wat': None}
        found = score_devices_to_ned_ids({'wat': {YangModule('blah')}}, self.neds)
        self.assertEqual(expected, find_best_ned_id(found))

    def test_fuzzy_match(self):
        expected = {'vmx': 'juniper-junos-nc-4.5:juniper-junos-nc-4.5'}

        device = {'vmx': {YangModule(':candidate:1.0'),
                          YangModule(':url:1.0'),
                          YangModule(':validate:1.0'),
                          YangModule('http://tail-f.com/ns/netconf/inactive/1.0'),
                          YangModule('http://xml.juniper.net/dmi/system/1.0'),
                          YangModule('http://xml.juniper.net/netconf/junos/1.0'),
                          YangModule('http://xml.juniper.net/xnm/1.1/xnm'),
                          YangModule('urn:ietf:params:xml:ns:netconf:base:1.0'),
                          YangModule('urn:ietf:params:xml:ns:netconf:capability:candidate:1.0'),
                          YangModule('urn:ietf:params:xml:ns:netconf:capability:confirmed-commit:1.0'),
                          YangModule('urn:ietf:params:xml:ns:netconf:capability:url:1.0?protocol=http,ftp,file'),
                          YangModule('urn:ietf:params:xml:ns:netconf:capability:validate:1.0'),
                          YangModule('urn:juniper-rpc')}}
        self.neds['juniper-junos-nc-4.5:juniper-junos-nc-4.5'] = {YangModule('http://xml.juniper.net/xnm/1.1/xnm', 'junos'),
                                                                  YangModule('urn:juniper-rpc', 'junos-rpc')}
        found = score_devices_to_ned_ids(device, self.neds)
        self.assertEqual(expected, find_best_ned_id(found))

    def setUp(self):
        cur_dir = Path(__file__).parent
        self.neds = _extract_ned_ids(str(cur_dir / 'ned-ids.xml'))
        self.devices = {}
        for cf in cur_dir.glob('capabilities-*.xml'):
            device = re.match(r'capabilities-(.*)\.xml', cf.name).groups()[0] # type: ignore
            self.devices[device] = _extract_device_capabilities(str(cur_dir / cf))
