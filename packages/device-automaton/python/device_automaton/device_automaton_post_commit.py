import traceback
from typing import Dict, List

import ncs

try:
    from ncs.cdb import OperSubscriber, Subscriber  # type: ignore
except ImportError:
    from ncs.experimental import Subscriber, OperSubscriber # type: ignore

from . import enqueue_work, service_log
from .common import classes, utils


class PostCommitSubscriberIter(classes.SubscriberIterSkipSlave):
    """Callbacks for CDB subscriber reacting to device automaton commit operations.

    The device automaton post-commit subscriber reacts to modifications of the device automaton
    instance by automatically executing the 'enqueue-work' action whenever the inputs change.
    This is convenient in case the device automaton has stopped for example due to invalid credentials.
    After the credentials are fixed, the next step is retried immediately.

    In addition to modifications the subscriber also handles delete operations. When the automaton
    service instance is removed, the /devices/delete-device-automaton action is executed."""

    def __init__(self, log):
        self.log = classes.LoggerWrapper(log, 'device-automaton-post-commit')

    def register(self, subscriber: Subscriber):
        subscriber.register('/ncs:devices/devaut:automaton', priority=101, iter_obj=self)

    def pre_iterate(self) -> Dict[str, int]:
        return {}

    def iterate(self, keypath, operation, oldval_unused, newval_unused, state: Dict[str, int]):
        # We are interested in modification notifications for the /ncs:devices/devaut:automaton{X} list
        # entry. We also want to ignore re-deploys occurring naturally during the RFM cycle.

        # The state dict contains the device automaton service instance name with a boolean
        # "interest" value. Initially, we set the value to False, but if the service is
        # "interesting", value is changed to True

        # Keypath is a reversed list of components. /ncs:devices/devaut:automaton{dut-r1}
        # becomes [('dut-r1',), 'devaut:automaton', 'ncs:devices']
        # Our first indication of an "interesting" notification occurs when a create or delete
        # operation is made on the device automaton service instance itself (len(kp)==3)
        if len(keypath) == 3:
            if operation in (ncs.MOP_CREATED, ncs.MOP_DELETED):
                state[str(keypath[0][0])] = operation
                return ncs.ITER_CONTINUE
            else:
                return ncs.ITER_RECURSE
        else:
            # This is the MOP_MODIFIED branch.
            # The service is not "interesting" if the only leaves that were changed are in the
            # /ncs:devices/devaut:automaton/private container. For instance, if the service is re-deployed,
            # only /ncs:devices/devaut:automaton/private/re-deploy-counter will be modified. Depending on who
            # executed the re-deploy, latest-u-info may also be changed.
            # We keep track of changes outside of the `private` container to determine if the service
            # is "interesting".
            if str(keypath[-4]) != 'private':
                # The service instance name is the third component from the right
                state[str(keypath[-3][0])] = ncs.MOP_MODIFIED
                return ncs.ITER_UP
            return ncs.ITER_RECURSE

    def should_post_iterate(self, state):
        return state

    def post_iterate(self, state: Dict[str, int]):
        try:
            with utils.maapi_session('python-devaut-service-post-commit', 'system') as m:
                for service_name, operation in state.items():
                    service_kp = f'/ncs:devices/devaut:automaton{{{service_name}}}'
                    try:
                        if operation == ncs.MOP_DELETED:
                            self.log.info(f'{service_kp}: delete-device-automaton')
                            # For delete operations, the service instance doesn't exist,
                            # so execute the action on the global '/ncs:devices' node
                            action = ncs.maagic.get_node(m, '/ncs:devices/devaut:delete-device-automaton')
                            action_input = action.get_input()
                            action_input.name = service_name
                            action(action_input)
                        else:
                            # For create and modify operations, run the enqueue-work action and clear backoff
                            # timers
                            self.log.info(f'{service_kp}: enqueue-work')
                            if operation == ncs.MOP_MODIFIED:
                                # Remove backoff-period and disable-until to react to failures after change immediately
                                with m.start_write_trans(db=ncs.OPERATIONAL) as t_write:
                                    service = ncs.maagic.get_node(t_write, service_kp)
                                    service.service_retrier.delete()
                                    self.log.debug('{}: service retrier backoff reset'.format(service_kp))
                                    t_write.apply()

                            service = ncs.maagic.get_node(m, service_kp)
                            result = enqueue_work.action(service, enqueue_work.InternalSource.POST_COMMIT)
                            # Execute the enqueue-work action to trigger the device work function

                            # The result object contains the results of the enqueue-work action
                            if result.success:
                                log_message = 'successfully executed enqueue-work after change'
                                self.log.debug('{}: {}'.format(service_kp, log_message))
                            else:
                                log_message = 'enqueue-work retry error {}'.format(result.message or '')
                                self.log.warning('{}: {}'.format(service_kp, log_message))
                            if operation == ncs.MOP_MODIFIED:
                                with m.start_write_trans(db=ncs.OPERATIONAL) as t_write:
                                    service = ncs.maagic.get_node(t_write, service_kp)
                                    service_log.log(service,
                                                    entry_type='devaut:service-retried',
                                                    message=log_message,
                                                    level='debug' if result.success else 'warn')
                                    t_write.apply()
                    except Exception as e:
                        self.log.error('post_iterate error {}: {}\n{}'.format(service_kp, e, traceback.format_exc()))
        except Exception as e:
            self.log.error('post_iterate error: {}\n{}'.format(e, traceback.format_exc()))


class PostCommitOperPlanSubscriberIter(classes.SubscriberIterSkipSlave):
    """Callbacks for oper CDB subscriber reacting to re-deploy-trigger leaf changes

    The device automaton oper post-commit plan subscriber reacts changes in the device automaton
    re-deploy-trigger leaf. When the leaf is set to true, the subscriber will flip it back
    to false.
    """

    def __init__(self, log):
        self.log = classes.LoggerWrapper(log, 'device-automaton-oper-plan-post-commit')

    def register(self, subscriber: OperSubscriber):
        subscriber.register('/ncs:devices/devaut:automaton/re-deploy-trigger', priority=102, iter_obj=self)

    def pre_iterate(self) -> List[str]:
        return []

    def iterate(self, keypath, operation_unused, oldval_unused, newval: ncs.Value, state):
        # if the value of the re-deploy-trigger leaf is True, append the leaf keypath to the state list
        if newval.as_pyval():
            state.append(str(keypath))

    def should_post_iterate(self, state):
        return state

    def post_iterate(self, state):
        try:
            with utils.maapi_session('python-devaut-service-oper-post-commit', 'system') as m:
                for kp in state:
                    try:
                        self.log.debug('setting {} to false'.format(kp))
                        with m.start_write_trans(db=ncs.OPERATIONAL) as oper_t_write:
                            oper_t_write.set_elem("false", kp)
                            oper_t_write.apply()
                    except Exception as e:
                        self.log.error('post_iterate error {}: {}\n{}'.format(kp, e, traceback.format_exc()))
        except Exception as e:
            self.log.error('post_iterate error: {}\n{}'.format(e, traceback.format_exc()))


class PostCommitSubscriber(Subscriber):
    def start(self):
        iterator = PostCommitSubscriberIter(log=self.log)
        iterator.register(self)
        super().start()


class PostCommitOperPlanSubscriber(OperSubscriber):
    def start(self):
        iterator = PostCommitOperPlanSubscriberIter(log=self.log)
        iterator.register(self)
        super().start()
