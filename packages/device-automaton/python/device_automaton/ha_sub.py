# -*- mode: python; python-indent: 4 -*-
"""An application that monitors HA events and provides in memory access to
other modules/applications.

This application read existing HA state, then monitors HA events.

The alternative to read the HA state directly has a high overhead and slows
down the performance of all applications.

WARNING: if other application/module using this module is not running in the
same Python VM, the call to is_ha_master_or_no_ha() will incur the same overhead
as read_state().
"""
import select
import socket
import threading
import traceback
from typing import Optional

from _ncs import events
import ncs
from bgworker.background_process import WaitableEvent

def is_ha_master_or_no_ha() -> bool:
    if not HaEventListener.listener_running:
        # read state directly if the listener is not running
        HaEventListener.read_state()

    return (HaEventListener.ha_status is None \
            or HaEventListener.ha_status == 'master')


class HaEventListener(threading.Thread):
    """HA Event Listener
    HA events, like HA-mode transitions, are exposed over a notification API.
    See bgworker.background_process for original implementation.
    """
    # current HA status:
    #   None: HA not enabled
    #   'none': HA enabled but neither master nor slave
    #   'master': HA Master
    #   'slave': HA Slave
    ha_status: Optional[str] = 'none'
    listener_running = False
    def __init__(self, app):
        super().__init__(name='ha_sub')
        self.app = app
        self.log = app.log
        self.log.info('HaEventListener: init')
        self.exit_flag = WaitableEvent()

    @staticmethod
    def read_state():
        # Read current status
        with ncs.maapi.single_read_trans('python-ha-state-sub', 'system', db=ncs.OPERATIONAL) as t:
            if t.exists("/tfnm:ncs-state/tfnm:ha"):
                HaEventListener.ha_status = str(ncs.maagic.get_node(t, '/tfnm:ncs-state/tfnm:ha/tfnm:mode'))
            else:
                HaEventListener.ha_status = None


    def run(self):
        self.app.add_running_thread(self.name + ' (HA event listener)')

        self.log.info('run() HA event listener')
        event_socket = socket.socket()
        events.notifications_connect(event_socket, events.NOTIF_HA_INFO,
                                     ip='127.0.0.1', port=ncs.PORT)

        # read initial state before listening to events
        HaEventListener.read_state()
        HaEventListener.listener_running = True
        while True:
            # monitor HA and Exit events
            try:
                rl, _, _ = select.select([self.exit_flag, event_socket], [], [])
                if self.exit_flag in rl:
                    event_socket.close()
                    return

                notification = events.read_notification(event_socket)
                ha_notif_type = notification['hnot']['type']
                old_state = HaEventListener.ha_status
                if ha_notif_type == events.HA_INFO_IS_MASTER:
                    HaEventListener.ha_status = 'master'
                elif ha_notif_type == events.HA_INFO_SLAVE_INITIALIZED:
                    HaEventListener.ha_status = 'slave'
                elif ha_notif_type in (events.HA_INFO_IS_NONE, events.HA_INFO_NOMASTER):
                    HaEventListener.ha_status = 'none'
                self.log.info(f'Processed HA event type {ha_notif_type}, old state {old_state}, new state {HaEventListener.ha_status}')

            except Exception as e:
                # not sure what kind of exception we will get,
                # set ha status to "none" to be on the safe side
                HaEventListener.ha_status = 'none'
                HaEventListener.listener_running = False
                self.log.error(f'Failed to process HA event, error {e}')
                self.log.error(traceback.format_exc())
                return


    def stop(self):
        self.exit_flag.set()
        self.join()
        self.app.del_running_thread(self.name + ' (HA event listener)')


class HaCheckerApp(ncs.application.Application):
    """HA Status Checker"""

    ha_event_listener: HaEventListener

    def setup(self):
        self.log.info(f"{__name__} setup()")

        # start the HA event listener thread
        self.ha_event_listener = HaEventListener(app=self)
        self.ha_event_listener.start()

    def teardown(self):
        self.log.info(f"{__name__} teardown()")
        self.ha_event_listener.stop()
