import concurrent.futures
import logging
from collections import namedtuple

FutureWorkItem = namedtuple('FutureWorkItem', ['data', 'retry_count'])


def concurrent_worker(items, work_fn, done_fn=None, error_fn=None,
                      max_workers=3, auto_retry=True, log=None, verbose=False):
    """Execute work_fn on items in max_workers background threads with automatic restart.

    :param items: list/generator of work items
    :param work_fn: work function, gets item, returns result
    :param done_fn: done function, gets work item, result and retry count, returns work result and retry flag
    :param error_fn: error function, gets work item and exception, returns work result and retry flag
    :param max_workers: max number of workers
    :param auto_retry: enable automatic retry of failed items
    :param log: logger
    :param verbose: log work items and results (developer flag)
    :return dict: a dictionary of original work items and their work results

    Uses concurrent.features module to execute work_fn on items in a ThreadPoolExecutor.

    The work_fn function will get the work item as the first argument and must return the result.
    For instance lambda item: math.pow(item, 2)

    The optional done_fn will be executed after the work is done. It will get the work item, the result
    and current retry count as arguments, and must return a tuple of result data and retry flag.
    For instance:

        def done_fn(item, result, retry_count):
            if result.success:
                # do something
                return result, False
            else:
                return result, True if retry_count < 5 else False

    The optional error_fn will be executed after the work is done but an exception was unhandled.
    It will get the work item, the exception and the current retry count as arguments, and must return a
    tuple of result data and retry flag.
    For instance:

        def error_fn(item, exc, retry_count):
            return None, True if item.retry_count < 5 else False

    After all the work is processed, the function will return a dict of original work items and their
    work results.
    """

    if log is None:
        logging.basicConfig(level=logging.DEBUG)
        log = logging.getLogger(__name__)

    name = work_fn.__name__

    with concurrent.futures.ThreadPoolExecutor(max_workers=max_workers) as executor:
        log.debug("Starting concurrent_worker using up to {} workers".format(max_workers))
        futures = {executor.submit(work_fn, item): FutureWorkItem(item, 0) for item in items}
        results = {}

        restart = True
        while restart:
            log.info('concurrent_worker({}): Restarting'.format(name))
            log.debug('concurrent_worker({}): Waiting for {} futures'.format(name, len(futures)))

            restart = False
            for future in concurrent.futures.as_completed(futures):
                item = futures[future]

                future_result = None
                retry = auto_retry
                try:
                    data = future.result()
                except Exception as exc:
                    log.error('concurrent_worker({}): {} generated an exception: {}'.format(name, item, exc))
                    if callable(error_fn):
                        future_result, retry = error_fn(item.data, exc, item.retry_count)
                else:
                    if verbose:
                        log.debug('concurrent_worker({}): {} result {}'.format(name, item, data))
                    if callable(done_fn):
                        future_result, retry = done_fn(item.data, data, item.retry_count)
                    else:
                        future_result = data
                        retry = False

                if retry:
                    item = FutureWorkItem(item.data, item.retry_count + 1)
                    if verbose:
                        log.debug('concurrent_worker({}): resubmitting {}'.format(name, item))
                    futures[executor.submit(work_fn, item.data)] = item
                    restart = True
                else:
                    results[item.data] = future_result

                del futures[future]

        return results


if __name__ == '__main__':
    import time

    def kajigger(whatchacallit):
        time.sleep(whatchacallit / 2)
        return whatchacallit**2

    concurrent_worker(range(1, 10), kajigger, done_fn=lambda x, y: (y, x.retry_count < 1))
