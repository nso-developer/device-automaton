import contextlib
import random
import re
import string
from datetime import datetime
from typing import List, overload, Optional

import _ncs
import ncs


@overload
def convert_yang_date_and_time_to_datetime(yang_date_and_time: None) -> None:
    ...

@overload
def convert_yang_date_and_time_to_datetime(yang_date_and_time: str) -> datetime:
    ...

def convert_yang_date_and_time_to_datetime(yang_date_and_time):
    """Convert a timestamp stored in a leaf of type yang:date-and-time to python datetime"""
    if not yang_date_and_time:
        return None
    # the timestamps are read as 2018-02-20T13:15:14+00:00
    # strip the extra +00:00 from the end
    yang_date_and_time = re.sub(r'[+-]\d{2}:\d{2}$', '', yang_date_and_time)
    if re.search(r'\.\d+$', yang_date_and_time):
        fmt = '%Y-%m-%dT%H:%M:%S.%f'
    else:
        fmt = '%Y-%m-%dT%H:%M:%S'
    return datetime.strptime(yang_date_and_time, fmt)


def format_yang_date_and_time(timestamp: Optional[datetime]=None, microseconds=True) -> str:
    """Format a timestamp in yang:date-and-time format

    :param timestamp: optional datetime instance. Defaults to utcnow()
    :param microseconds: whether to include microseconds in the timestamp. Defaults to True
    :return: yang:date-and-time formatted string"""
    if not timestamp:
        timestamp = datetime.utcnow()
    elif not isinstance(timestamp, datetime):
        raise ValueError('timestamp must be datetime instance')
    if microseconds:
        fmt = '%Y-%m-%dT%H:%M:%S.%fZ'
    else:
        fmt = '%Y-%m-%dT%H:%M:%SZ'
    return timestamp.strftime(fmt)


def decrypt_encrypted_string(node, value):
    """Decrypts an encrypted tailf:aes-cfb-128-encrypted-string type leaf

    :param node: any maagic Node
    :param value: the encrypted leaf value
    """
    if value is None:
        return None

    if isinstance(node._backend, ncs.maagic._TransactionBackend):
        node._backend.maapi.install_crypto_keys()
    elif isinstance(node._backend, ncs.maagic._MaapiBackend):
        node._backend.install_crypto_keys()
    else:
        raise ValueError("Unknown MaagicBackend for leaf")

    return _ncs.decrypt(value) #pylint: disable=no-member


def prepare_template_variables(values):
    """Create ncs.template.Variables from a given dict.

    Capitalizes variable names and uses empty strings for None values."""
    tvars = ncs.template.Variables()
    keys: List[str] = []
    for k, v in values.items():
        if k.upper() in keys:
            raise ValueError('Duplicate key ' + k.upper())
        tvars.add(k.upper(), v or '')
        keys.append(k.upper())
    return tvars


def read_ned_id(device: ncs.maagic.Container) -> str:
    device_type = device.device_type
    ned_type = str(device_type.ne_type)
    case = getattr(device_type, ned_type)
    return case.ned_id


def prune_device_from_queue_items(log, maapi, device, uinfo):
    """Helper method for pruning a given device from all queue-items

    This method will remove the given device from all queue-items using the /devices/commit-queue/prune action.
    Note that it will not have an effect on devices that are currently being committed.

    NSO 4.6.1 introduces two changes regarding commit queues:
    - commit queue items cannot be removed if they're still executing. We have to use
    the 'force' parameter.
    - by default, commit queue items are created with "atomic behaviour" enabled. Individual devices
    cannot be pruned from an atomic queue item

    :param log: log handler
    :param maapi: maapi session
    :param device: device name
    :param uinfo: UserInfo instance (for setting action timeout)
    """

    # first, find all commit-queue items using this device, and set them to non-atomic
    queue_items = []

    with maapi.start_read_trans(db=ncs.OPERATIONAL) as trans:
        def find_queue_items(kp, _):
            try:
                queue_items.append(ncs.maagic.get_node(trans, kp))
            except KeyError:
                # Normally queue items do not disappear after completion, except
                # for the special items created by NSO device locks. These are
                # implemented with the commit-queue as special queue items without any
                # work associated with them. These locks are created as part of device
                # interactions, the most common one being sync-from. After the
                # lock is release the queue item vanishes from the list.
                # In addition to these vanishing queue items this except block
                # also handles the device list entry itself disappearing.
                pass

        # The special "device lock queue items" have the tag that starts with
        # "device lock for ...". We do not exclude them from the search because
        # we must still wait for completion.
        trans.xpath_eval('/devices/commit-queue/queue-item[devices="{}"]'.format(device), find_queue_items, trace=None, path='')

        if len(queue_items) == 0:
            log.debug(f'/ncs:devices/devaut:automaton{{{device}}}: skipped commit queue pruning')
            return

        root = ncs.maagic.get_root(trans)
        if root.devices.device[device].write_timeout:
            write_timeout = root.devices.device[device].write_timeout
        else:
            write_timeout = root.devices.global_settings.write_timeout

        needs_pruning = False
        for queue_item in queue_items:
            # clear maagic node cached values
            queue_item._set_attr('_populated', None)
            try:
                if queue_item.status not in ('completed', 'failed', 'deleted'):
                    _ncs.dp.action_set_timeout(uinfo, write_timeout * 2)
                    log.debug('{}: item status {}, age {}, waiting for {}s'
                            .format(queue_item._path, queue_item.status,
                                    queue_item.age, write_timeout))
                    wait_input = queue_item.wait_until_completed.get_input()
                    wait_input.timeout = write_timeout
                    wait_result = queue_item.wait_until_completed(wait_input)
                    if wait_result.result != 'completed':
                        needs_pruning = True
                # clear maagic node cached values
                queue_item._set_attr('_populated', None)
                log.debug('{}: item status {}, age {}'.format(queue_item._path, queue_item.status, queue_item.age))

                if queue_item.is_atomic and queue_item.status != 'completed':
                    log.info('{}: setting atomic behaviour to false'.format(queue_item._path))
                    atomic_input = queue_item.set_atomic_behaviour.get_input()
                    atomic_input.atomic = False
                    queue_item.set_atomic_behaviour(atomic_input)
                    needs_pruning = True
            except ncs.error.Error as e:
                if 'item does not exist' in str(e):
                    log.debug(f'{queue_item._path}: item no longer exists')
                    continue
                else:
                    raise

        if needs_pruning:
            # next, we're able to prune the device from all items
            prune_action = root.devices.commit_queue.prune
            prune_input = prune_action.get_input()
            prune_input.device = [device]
            # in NSO 4.6.1, we can use 'force' to "... brutally kill an ongoing commit"
            # since we're removing the device, we don't care about the state of the device afterwards
            prune_input.force.create()
            result = prune_action(prune_input)

            log.debug('Pruned device {} from {} queue-items'.format(device, result.num_matched_queue_items))

        log.debug('/ncs:devices/devaut:automaton{{{}}}: completed commit queue pruning'.format(device))


def pl2pd(pl):
    """proplist to propdict
    Who came up with the silly idea of a proplist? A list of tuples where the
    first part of the tuple can be considered a key and the second a value...
    that is much more naturally handled as a dict, so we can convert the
    proplist to a dict. Make sure to use the inverse pd2pl before you return!

    Also, all keys and values must be strings! We don't convert here, better
    make sure you do!
    """
    r = {}
    for n, m in pl:
        r[n] = m
    return r


def pd2pl(d):
    """propdict to proplist
    The inverse of pl2pd. Go read about it and why proplists are silly.
    """
    r = list()
    for k, v in d.items():
        r.append((k, v))
    return r


def random_string(string_length):
    """Generate a random string of letters and digits"""
    letters_and_digits = string.ascii_letters + string.digits
    return ''.join(random.choice(letters_and_digits) for i in range(string_length))


@contextlib.contextmanager
def maapi_session(username: str, context: str):
    with ncs.maapi.Maapi() as m:
        with ncs.maapi.Session(m, username, context):
            yield m
