from typing import Optional

import ncs
from alarm_sink import alarm_sink


def _create_device_automaton_alarm_object(maapi: ncs.maapi.Maapi, device: str, failed: bool, management_endpoint: Optional[str] = None):
    """Create Alarm object for device automaton instance

    Will populate all necessary fields for device automaton instance or management endpoint alarm.

    :param device: string device name
    :param failed: boolean device state
    :param management_endpoint: optional management endpoint for alarm resource
    :return: alarm_sink.Alarm object"""
    mo = "/ncs:devices/devaut:automaton[devaut:device='{}']".format(device)
    if management_endpoint:
        mo += "/devaut:management-endpoint[devaut:address='{}']".format(management_endpoint)

    impacted_services = None
    if management_endpoint:
        alarm_type = 'devaut:management-endpoint-unreachable'
        severity = alarm_sink.PerceivedSeverity.MINOR
        if failed:
            alarm_text = 'Management endpoint {} down'.format(management_endpoint)
        else:
            alarm_text = 'Management endpoint {} recovered'.format(management_endpoint)
    else:
        alarm_type = 'devaut:device-unreachable'
        severity = alarm_sink.PerceivedSeverity.MAJOR
        if failed:
            alarm_text = 'Device {} unreachable'.format(device)
        else:
            alarm_text = 'Device {} recovered'.format(device)
        impacted_services = _find_impacted_service_objects(maapi, device)

    alarm_id = alarm_sink.AlarmId(device=device, managed_object=mo,
                                  type=alarm_type,
                                  specific_problem=None)
    # only find service instances related to device for device-unreachable alarm
    alarm = alarm_sink.Alarm(alarm_id,
                             severity=severity,
                             alarm_text=alarm_text,
                             impacted_objects=impacted_services)
    alarm.cleared = not failed

    return alarm

def _find_impacted_service_objects(maapi: ncs.maapi.Maapi, device: str):
    with maapi.start_read_trans(db=ncs.OPERATIONAL) as t_read:
        root = ncs.maagic.get_root(t_read)
        # service-list is a leaf-list maagic node, return a python list
        try:
            dev_service = root.ncs__devices.devaut__automaton[device]
            if dev_service.populate_impacted_objects_in_alarm:
                return root.devices.device[device].service_list.as_list()
        except KeyError:
            # the device list entry may not actually exist (device is ncs)
            pass

    # attempting to get the affected services by looking at backpointers in device config
    # this doesn't seem to work though ...
    # with maapi.start_read_trans() as t:
    #     config = ncs.maagic.get_node(t, '/devices/device{dut-r1-1}/config')
    #     for attr_name in dir(config):
    #         if attr_name.startswith('__'):
    #             continue
    #         child_attr = getattr(config, attr_name)
    #         if child_attr:
    #             path = '{}/{}'.format(config._path, attr_name.replace('__', ':').replace('_', '-'))
    #             print('Checking', path)
    #             print(t.get_attrs([0x80000003, 0x80000001], path))


def submit_device_automaton_alarm(ask, device, failed, management_endpoint=None):
    """Submit a change for a device automaton alarm

    :param ask: AlarmSink instance
    :param device: string device name
    :param failed: boolean device state
    :param management_endpoint: optional management endpoint for alarm resource"""

    alarm = _create_device_automaton_alarm_object(ask.maapi, device, failed, management_endpoint)
    ask.submit_alarm(alarm)


def clear_device_automaton_alarm(ask, device, management_endpoint=None):
    """Forcibly clear a device automaton alarm

    :param ask: AlarmSink instance
    :param device: string device name
    :param management_endpoint: optional management endpoint for alarm resource"""

    alarm = _create_device_automaton_alarm_object(ask.maapi, device, False, management_endpoint)
    alarm.alarm_text = 'Automatically cleared due to resource removal'
    ask.submit_alarm(alarm)


def clear_device_alarms(maapi, device: str):
    """Forcibly clear all alarms where the device is part of the alarm-id

    :param maapi: a MAAPI session
    :param device: string device name
    """
    with maapi.start_read_trans(db=ncs.OPERATIONAL) as oper_t_read:
        ask = alarm_sink.AlarmSink(maapi)
        with ncs.experimental.Query(oper_t_read,
                                    expr=f'alarm[device="{device}"][is-cleared="false"]',
                                    context_node='/al:alarms/alarm-list',
                                    select=['managed-object', 'type', 'specific-problem', 'severity'],
                                    result_as=ncs.QUERY_STRING) as q:
            for ale in q:
                alarm_id = alarm_sink.AlarmId(device, *ale[:3])
                ask.clear_alarm(alarm_id, alarm_text='Automatically cleared due to resource removal')
