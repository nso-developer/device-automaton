from enum import Enum

import ncs


class InternalSource(Enum):
    RETRIER = 'retrier'
    POST_COMMIT = 'post-commit'
    MONITOR = 'monitor'
    CHECK_SYNC = 'check-sync'


def action(automaton: ncs.maagic.Node, source: InternalSource) -> ncs.maagic.Node:
    action_input = automaton.enqueue_work.get_input()
    action_input.internal_source = source.value
    return automaton.enqueue_work(action_input)
