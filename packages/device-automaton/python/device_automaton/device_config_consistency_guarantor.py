"""The consistency guarantor periodically checks that actual configuration on
devices is in sync with the cached copy of the configuration held in NSO.

All work happens from a thread running in the background defined here, so start
by reading that and then see the helper functions in device_check_sync.py.

The overall implementation is optimised to be as "gentle" as possible, not
introducing CPU spikes etc. This is accomplished by spreading out the checking
of devices over time. We want to keep the max latency for transactions towards
the north bound NETCONF interface of NSO down as low as possible which is why we
are taking this approach.

While it is possible to execute global check-sync on all devices managed by NSO,
the operation locks up to 100 devices in parallel and then keeps a rolling
window while closing connections to processed devices. We sacrifice total
efficiency (like doing check-sync on 50 devices in parallel might be more
efficient) but we keep down the latency by allowing interleaving of northbound
queries with these check sync operations.

The actual implementation spreads the check-sync operation on individual
devices across the globally configured polling-period.
For example, if the global polling-period is 60, three devices will be checked
in the next 60 seconds, with 20 seconds of wait time between devices.

Some devices may also have a specific polling-period. These will be checked
at the specified intervals.
"""

import dataclasses
from datetime import MINYEAR, datetime, timedelta
import itertools
import logging
import math
import time
import traceback
from typing import List, Optional, Tuple, Union

import ncs
from maagic_copy.maagic_copy import maagic_copy

from .common import utils
from .device_check_sync import DeviceCheckSync


@dataclasses.dataclass(frozen=True)
class SkippedDevice:
    name: str
    skip_reason: str


class DeviceCheckSchedule:
    def __init__(self, name: str, last_check_time: Optional[str],
                 device_polling_period: Optional[int], global_polling_period: int) -> None:
        self.name = name
        self.polling_period = device_polling_period or global_polling_period
        # mypy incorrectly moans about incompatible types: Expression has type
        # "Optional[str]", variable has type "datetime". We use the
        # last_check_time property setter to handle multiple source types,
        # including None. The setter is therefore annotated with a different
        # type than the property: https://github.com/python/mypy/issues/3004
        self.last_check_time = last_check_time  # type: ignore
        self.has_global_polling_period = not device_polling_period

    @property
    def last_check_time(self) -> datetime:
        return self._last_check_time

    @last_check_time.setter
    def last_check_time(self, value: Union[datetime, str, None]):
        if isinstance(value, datetime):
            self._last_check_time = value
        elif value:
            self._last_check_time = utils.convert_yang_date_and_time_to_datetime(value)
        else:
            self._last_check_time = datetime(MINYEAR, 1, 1)
        self.next_check_time = self._calculate_next_check_time(self._last_check_time, self.polling_period)

    @staticmethod
    def _calculate_next_check_time(last_check_time: datetime, device_polling_period: int) -> datetime:
        """Given last-check-time, calculate next time using device specific / global polling-period"""
        return last_check_time + timedelta(seconds=device_polling_period)

    def __repr__(self):
        return 'DeviceCheckSchedule name={self.name} next_check_time={self.next_check_time} last_check_time={self.last_check_time} polling_period={self.polling_period}'.format(self=self)


def _get_devices_with_next_check_time(maapi, global_polling_period) -> Tuple[List[DeviceCheckSchedule], List[SkippedDevice]]:
    """
    Prepare the device check schedule sorted in ascending order by next_check_time, and a
    list of skipped devices.
    """
    devices = []
    skipped_devices = []
    with maapi.start_read_trans(db=ncs.OPERATIONAL) as t_read:
        root = ncs.maagic.get_root(t_read)
        for device in root.ncs__devices.devaut__automaton:
            skip_reason = None
            if not device.consistency_guarantor.enabled:
                skip_reason = 'consistency-guarantor disabled'
            # skip failed devices (sync-from will be performed automatically after recovery)
            elif device.plan.failed or not device.device_ready or not device.alive:
                skip_reason = 'device automaton not ready'
            if skip_reason:
                skipped_devices.append(SkippedDevice(device.device, skip_reason))
            else:
                last_checked = device.device_check_sync_state.last_checked_config
                device_polling_period = device.consistency_guarantor.polling_period
                devices.append(DeviceCheckSchedule(device.device, last_checked,
                                                   device_polling_period, global_polling_period,))

    devices.sort(key=lambda x: x.next_check_time)
    return devices, skipped_devices


def _work_cycle(maapi, log, polling_period: float, take_devices: int) -> float:
    """Perform one cycle of check-sync operations, on n devices

    Will first check when the device was last checked:
    - never: immediately check n first devices
    - some time ago: check oldest n devices

    After performing the check, return the calculated time to wait until the
    next cycle: divide the global polling-period with the number of devices"""

    # get a list of devices with next check time, ascending order by time
    devices, skipped_devices = _get_devices_with_next_check_time(maapi, polling_period)

    if not devices:
        timeout = polling_period
        log.debug('DeviceConfigConsistencyGuarantor: No device automaton instances found')
        return timeout

    # eligible devices have next_check_time before now
    now = datetime.utcnow()
    eligible_devices = list(
        itertools.islice((device for device in devices if device.next_check_time <= now),
                         take_devices))

    # if no eligible_devices found, wait until the next eligible device can be checked
    if not eligible_devices:
        timeout = math.ceil((devices[0].next_check_time - now).total_seconds())
        log.debug('DeviceConfigConsistencyGuarantor: next eligible device check in {} seconds'.format(timeout))
        return timeout

    start = datetime.utcnow()

    dcs = DeviceCheckSync(maapi, log)
    results = dcs.device_check_sync(device.name for device in eligible_devices)
    dcs.results_to_cdb_state(results)
    dcs.results_to_reaction(results)

    # for the devices that were just checked, update the last_check_time to now
    # this is an estimation, and saves us another lookup for the actual time from CDB
    now = datetime.utcnow()

    for i in range(len(eligible_devices)):
        devices[i].last_check_time = now
    devices.sort(key=lambda x: x.next_check_time)

    # the polling window is defined as an interval for evenly distributing devices with a
    # global polling-period, across the polling-period
    number_of_global_devices = len(list(d for d in devices if d.has_global_polling_period))
    if number_of_global_devices:
        avg_spacing = timedelta(seconds=polling_period / number_of_global_devices)
    else:
        avg_spacing = timedelta(seconds=polling_period)
    try:
        # are there any devices we need to check within the next polling window
        next_device = next(d for d in devices if d.next_check_time < now + avg_spacing and not d.has_global_polling_period)
    except StopIteration:
        # no devices found, wait for one polling window. in the next cycle the search statement
        # above may return something, or we will wait again
        log.debug('DeviceConfigConsistencyGuarantor: no explicitly scheduled devices to check')
        timeout = avg_spacing.total_seconds()
    else:
        # wait for a specific amount of time until the next required check
        log.debug('DeviceConfigConsistencyGuarantor: next_device={nd.name} due to be checked at {nd.next_check_time}'.format(nd=next_device))
        timeout = math.ceil((next_device.next_check_time - now).total_seconds())

    with maapi.start_write_trans(db=ncs.OPERATIONAL) as oper_write:
        root = ncs.maagic.get_root(oper_write)
        for device in devices:
            try:
                state = root.ncs__devices.devaut__automaton[device.name].device_check_sync_state
            except KeyError:
                continue
            state.next_check_time = utils.format_yang_date_and_time(device.next_check_time)
            state.planned_action = 'planned-check'
            state.skip_reason = None
        for skipped_device in skipped_devices:
            try:
                state = root.ncs__devices.devaut__automaton[skipped_device.name].device_check_sync_state
            except KeyError:
                continue
            state.next_check_time = None
            state.planned_action = 'skip'
            state.skip_reason = skipped_device.skip_reason

        # write the log
        end = datetime.utcnow()
        duration = (end - start).total_seconds()

        ccg_devices = root.ncs__devices.global_settings.devaut__automaton.configuration_consistency_guarantor
        poll_stats = ccg_devices.last_poll_stats
        poll_stats.start_timestamp = utils.format_yang_date_and_time(start)
        poll_stats.end_timestamp = utils.format_yang_date_and_time(end)
        poll_stats.total_duration = int(duration * 100)

        poll_stats.total_devices = len(devices) + len(skipped_devices)
        poll_stats.checked_devices = len(results)
        poll_stats.skipped_devices = len(skipped_devices)
        poll_stats.out_of_sync_devices = len([d for d in results.values() if d.sync_result == 'out-of-sync'])
        poll_stats.error_devices = len([d for d in results.values() if d.sync_result not in ('in-sync', 'out-of-sync')])

        # copy the same data to poll-stats-history
        poll_stats_history = ccg_devices.poll_stats_history.create(poll_stats.start_timestamp)

        maagic_copy(poll_stats, poll_stats_history)

        # prune old entries
        log_history_size = ccg_devices.global_log_history_size
        log_keys = ccg_devices.poll_stats_history.keys()
        if len(log_keys) > log_history_size:
            for log_key in log_keys[:len(log_keys) - log_history_size]:
                del ccg_devices.poll_stats_history[log_key]

        oper_write.apply()

    log.debug('DeviceConfigConsistencyGuarantor: Device config check finished, took {} seconds'.format(duration))
    log.debug('DeviceConfigConsistencyGuarantor: Going to wait for {} seconds'.format(timeout))
    return timeout


def run_device_ccg() -> None:
    log = logging.getLogger('DeviceConfigConsistencyGuarantor')
    log.info('Starting worker thread')

    timeout: float = 1
    with ncs.maapi.Maapi() as m:
        m.start_user_session('python-devaut-config-consistency', 'system')
        while True:
            time.sleep(timeout)
            try:
                with m.start_read_trans() as t:
                    settings = ncs.maagic.get_node(t, '/ncs:devices/global-settings/devaut:automaton/configuration-consistency-guarantor')
                    polling_period = settings.polling_period
                    checkers = settings.parallel_checkers
                timeout = _work_cycle(m, log, polling_period, checkers)
            except Exception as e:
                log.error('error in DeviceConfigConsistencyGuarantor', exc_info=True)
                log.error(traceback.format_exc())
                timeout = polling_period
