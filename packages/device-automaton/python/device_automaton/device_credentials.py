"""
Module to help juggle device credentials

This module includes the class DeviceCredentialJuggler, which is used by the
device automaton at various stages.

In the `setup-initial-credentials` state,
the DeviceCredentialJugger.setup_initial_device_credentials method is used to
discover and decide on the initial (factory) credentials. It does not
actually change any device configuration, just uses existing data in NSO to
create the device-specific authgroup.

In the `setup-management-credentials`, the
DeviceCredentialJuggler.setup_management_device_credentials method is used to
perform the credentials configuration in three steps:
    1. Figure out the appropriate management credentials (for order,
    see DeviceCredentialJuggler.collect_credentials. Create a user on the
    target device with these credentials.
    2. Update the device-specific authgroup in NSO with the new credentials.
    3. Remove old user from the target device.

A note on why does each device have its own authgroup: say you have several
devices using the same global management credentials. Due to the requirement
that NSO authgroup and device users need to be changed in separate
transactions, this would become a nightmare to orchestrate if the change
involved a lot of devices, some of which are offline.
If some devices are not available at the time, they will go through the
`setup-management-credentials` state anyway as they recover, so the changes
will be made then.
"""
import base64
import erlang
from collections import OrderedDict
from enum import StrEnum
from functools import wraps
from typing import Dict, Iterator, Optional, Tuple

import _ncs
import ncs
from . import TemplateName
from .common import classes, utils
from .nso6_trans_conflict_retry import *


class SshKeyAlgorithm(StrEnum):
    SSH_RSA = 'ssh-rsa'
    ECDSA_SHA2_NISTP256 = 'ecdsa-sha2-nistp256'
    SSH_ED25519 = 'ssh-ed25519'


class DeviceCredentials:
    """Wrapper for device credentials

    Only one of the password / (algorithm, private_key, public_key) is valid for an
    instance of this class.
    """

    def __init__(self, username, password=None, password_plain=None,
                 algorithm=None, public_key=None, private_key=None, private_key_plain=None,
                 path=None):
        self.username = username
        self.password = password
        self.password_plain = password_plain
        self.algorithm = algorithm
        self.public_key = public_key
        self.private_key = private_key
        self.private_key_plain = private_key_plain
        self.path = path
        assert not(password and private_key)

    def __str__(self):
        return f'''DeviceCredentials(username='{self.username}', password='{"***" if self.password else None}', algorithm='{self.algorithm}', private_key='{"***" if self.private_key else None}')'''

    def asdict(self):
        return {'username': self.username,
                'password': self.password,
                'algorithm': self.algorithm,
                'public_key': self.public_key,
                'private_key': self.private_key,
                'path': self.path}

    def __eq__(self, other):
        """Compare username, password and private_key values"""
        return self.username == other.username and \
               self.password_plain == other.password_plain and \
               self.algorithm == other.algorithm and \
               self.private_key_plain == other.private_key_plain

    def __hash__(self):
        # Must be defined to make the object hashable, since it is used in a dict.
        return id(self)

    @staticmethod
    def from_config(dc: ncs.maagic.Container) -> Iterator['DeviceCredentials']:
        """Helper factory for creating DeviceCredentials object from CDB structures

        Will generate up to two DeviceCredentials tuples for a given device,
        depending on password/private key availability. An SSH key has has
        precedence over a password."""
        if dc.username:
            for keypair in dc.keypair:
                private_key_plain = utils.decrypt_encrypted_string(dc, keypair.private_key)
                yield DeviceCredentials(dc.username, algorithm=SshKeyAlgorithm(str(keypair.algorithm)), public_key=keypair.public_key,
                                        private_key=keypair.private_key, private_key_plain=private_key_plain,
                                        path=dc._path)
            if dc.password:
                password_plain = utils.decrypt_encrypted_string(dc, dc.password)
                yield DeviceCredentials(dc.username, password=dc.password, password_plain=password_plain, path=dc._path)


def filter_cisco_ios_xr_ssh_key(collect_credentials):
    @wraps(collect_credentials)
    def wrap(self, device, mep=None):
        cisco_iosxr = 'http://cisco.com/ns/yang/cisco-xr-types' in self.root.ncs__devices.ncs__device[device].capability
        for dc in collect_credentials(self, device, mep):
            if dc.private_key and cisco_iosxr:
                continue
            yield dc
    return wrap


class DeviceCredentialJuggler(classes.DeviceTimeoutTaker, classes.Retrier):
    """Helps us juggle credentials
    """
    def __init__(self, root, maapi, log):
        self.root = root
        self.maapi = maapi
        self.log = log

    @filter_cisco_ios_xr_ssh_key
    def collect_credentials(self, device, mep=None) -> Iterator[DeviceCredentials]:
        """Generator method for collecting suitable management credentials for device

        :param device: device name
        :param mep: optional ManagementEndpoint tuple
        :return: DeviceCredentials tuple

        The credentials are returned in the following order:
            1. single device-specific management credentials (from device automaton)
            2. (optional) management-endpoint device-specfic initial credentials (from device automaton)

        Note that password and rsa key are two distinct credentials.
        """
        device_automaton = self.root.ncs__devices.devaut__automaton[device]

        yield from DeviceCredentials.from_config(device_automaton.management_credentials)

        if mep:
            for dc in device_automaton.management_endpoint[mep.address].initial_credentials:
                yield from DeviceCredentials.from_config(dc)

    def setup_initial_device_credentials(self, device: str) \
            -> Tuple[bool, Dict[DeviceCredentials, Optional[str]]]:
        """Initial (factory) device credentials discovery

        :param device: device name
        :returns: a tuple of (result, dict), where the ordered dict contains tested credentials
        with failure reasons

        Loop through available management endpoints, find the appropriate
        credentials and configure a device-specific NSO authgroup
        """
        ncs_device = self.root.devices.device[device]
        device_automaton = self.root.ncs__devices.devaut__automaton[device]
        attempted_credentials: Dict[DeviceCredentials, Optional[str]] = OrderedDict()

        for mep in device_automaton.management_endpoint:
            if not mep.alive:
                continue
            for dc in self.collect_credentials(device, mep):
                self._take_timeout(device, device_automaton)
                attempted_credentials[dc] = None
                self.log.debug('Testing credentials {}'.format(dc))
                self._create_device_authgroup(device, dc)

                success, error = self.test_credentials(device_automaton, ncs_device)

                if success:
                    self.log.debug('Found credentials {}'.format(dc))
                    # By storing the user in the current-management-user leaf
                    # we ensure the initial user is removed when we switch to
                    # final management credentials
                    with self.maapi.start_write_trans(db=ncs.OPERATIONAL) as t:
                        write_device_automaton = ncs.maagic.get_node(t, device_automaton._path)
                        write_device_automaton.current_management_user = dc.username
                        t.apply()
                    return True, attempted_credentials
                else:
                    attempted_credentials[dc] = error
        return False, attempted_credentials

    def check_configured_credentials(self, device: str, expected_credentials: DeviceCredentials, create_management_credentials: bool) -> bool:
        """Check if the configured credentials in the authgroup match expected"""

        current_credentials = get_device_credentials(self.root, device)
        if current_credentials is None:
            return False

        def decrypt(password):
            return utils.decrypt_encrypted_string(self.root, password)

        username_correct = current_credentials.username == expected_credentials.username
        if expected_credentials.private_key:
            secret_correct = decrypt(expected_credentials.private_key) == \
                             current_credentials.private_key and \
                             expected_credentials.algorithm == current_credentials.algorithm

        elif expected_credentials.password:
            secret_correct = decrypt(expected_credentials.password) == \
                             current_credentials.password
        else:
            secret_correct = False

        # The user-account service is not created if we're not allowed to create credentials
        if create_management_credentials:
            service_exists = expected_credentials.username in self.root.devices.device[device].devaut__user_account
        else:
            service_exists = True
        return username_correct and secret_correct and service_exists

    def test_credentials(self, device_automaton: ncs.maagic.Node, ncs_device: ncs.maagic.Node) -> Tuple[bool, str]:
        # This wrappers purpose is to add a timeout before executing /devices/device/connect action,
        # to make sure we're not blocked by IOS XR stupid rate limiter
        def connect_wrapper():
            self._take_timeout(device_automaton.device, device_automaton)
            return ncs_device.connect()

        # ncs_device.connect action returns a (result(boolean), info(string)) container.
        # The attempt may succeed/fail depending on the credentials used. But it may
        # also result in an error if:
        #  - the device SSH host key has changed,
        #  - the device is dead / busy / NSO can't reach it on this address.
        # If the device is unreachable right now, it makes sense to retry connecting
        # a couple of times before giving up.
        # In case of an authentication failure, retrying will not change the outcome.
        def result_eval_fn(result) -> Tuple[bool, Optional[str]]:
            if result.result:
                return True, None

            # abort-type error messages:
            # - "Failed to authenticate towards device X: Unknown SSH host key"
            # - "Failed to authenticate towards device X: Bad password for local/remote user admin/RTNMS"
            # - "Failed to authenticate towards device X: SSH host key mismatch"
            # - "Failed to connect to device X: connection refused: Password authentication failed. in new state"
            # - Failed to connect to device X: connection refused: NEDCOM CONNECT: Authenticate: Exhausted available authentication methods. Server allowed: [ publickey keyboard-interactive password ] in new state
            # We will return "success" to stop retrying, but the credential change operation
            # will fail.
            if 'Failed to authenticate' in result.info or 'authentication failed' in result.info or 'Exhausted available authentication methods' in result.info:
                self.log.warning('{}: connect authentication error: {}'.format(device_automaton._path, result.info))
                return True, result.info
            elif result.info.endswith('Failed to establish connection'):
                # The new versions of the alu-sr-cli NED (>= v8.15) return a
                # very generic and non-descriptive error message when the
                # connect action fails. The message is the same no matter what
                # the root cause is, whether the credentials are actually
                # incorrect or there was a connectivity problem. To avoid
                # hammering the mgmt interface with many failed attempts we
                # abort on the first failure.
                self.log.warning('{}: connect error (NED generic error): {}'.format(device_automaton._path, result.info))
                return True, result.info
            else:
                self.log.warning('{}: connect error: {}'.format(device_automaton._path, result.info))
                return False, result.info

        success, error = self._retry_function(connect_wrapper,
                result_eval_fn, suffix='test-initial-credentials on ' + device_automaton.device)

        # success=True is used as a shortcut to exit from the _retry_function when we immediately
        # know the credentials are wrong. error=None when correct credentials were found
        if success and error:
            success = False
        return success, error

    def setup_management_device_credentials(self, device) -> Tuple[bool, Optional[str]]:
        """Management credentials setup in the final stage of the device automaton

        Collect the first suitable credential, create user on the device,
        reconfigure device-specific NSO authgroup and finally remove the old
        credentials.
        These steps need to be performed in separate transactions, as NSO
        would otherwise try to use the new credentials to connect to the
        device in the same transaction as we would be creating the user on
        the device.
        """
        try:
            dc = next(self.collect_credentials(device))
        except StopIteration as e:
            return False, f'Management credentials for device {device} not found!'
        else:
            result = self._retry_function(self._create_user_account_service, args=[device, dc],
                                          suffix='create-user-device on ' + device)
            if not result[0]:
                return result

            self._create_device_authgroup(device, dc)

            result = self._retry_function(self._remove_old_management_user, args=[device, dc],
                                          suffix='remove-user-device on ' + device)
            return result

    @retry_on_conflict()
    def _create_user_account_service(self, device, dc):
        """Create/update the management user on device with user-account service

        The given credentials can be either password or rsa key. Even if the
        account entry in the user-account service instance already exists for
        the same username, the user-account service is force-redeployed. This
        may help in case the device configuration has changed, like after a
        factory reset. The transaction is committed with the "reconcile
        discard-non-service-config" flag. The user-account service becomes the
        sole owner of the user account, removing any additional config.
        """
        self.log.info('Setting device {} management credentials {}'.format(device, dc))
        with self.maapi.start_write_trans() as t_write:
            root = ncs.maagic.get_root(t_write)
            already_exists = dc.username in root.devices.device[device].devaut__user_account
            device_automaton = root.ncs__devices.devaut__automaton[device]
            if already_exists and device_automaton.management_credentials.password_or_ssh_key == 'password':
                del root.devices.device[device].devaut__user_account[dc.username].authorized_key
            template = ncs.template.Template(device_automaton)
            template.apply(TemplateName('devaut-user-account-service'))
            # force a re-deploy in case the user was removed from device
            if already_exists:
                root.devices.device[device].devaut__user_account[dc.username].private.re_deploy_counter += 1
            # We want to make sure the user exists on the device after this is
            # committed because we will attempt to use the new credentials in
            # the next step. If commit-queue is enabled on the device, bypass
            # it.
            t_write.apply(flags=ncs.maapi.COMMIT_NCS_BYPASS_COMMIT_QUEUE | ncs.maapi.COMMIT_NCS_RECONCILE_DISCARD_NON_SERVICE_CONFIG)

        # another re-deploy reconcile to reset refcounts to 1
        with self.maapi.start_read_trans() as t_read:
            root = ncs.maagic.get_root(t_read)
            uas = root.devices.device[device].devaut__user_account[dc.username]
            ri = uas.re_deploy.get_input()
            ri.reconcile.create()
            ri.reconcile.discard_non_service_config.create()
            ri.commit_queue.create()
            ri.commit_queue.bypass.create()
            self.log.debug(f're-deploy reconcile on {uas._path}')
            uas.re_deploy(ri)

    def _safe_delete(self, user_list, *keys):
        try:
            del user_list[keys]
            self.log.debug(f'Deleted user {keys} from {user_list._path}')
        except KeyError:
            self.log.debug(f'User {keys} not found in {user_list._path}')

    def _find_user_ordering_index(self, user_list: ncs.maagic.List, username: str) -> Optional[int]:
        try:
            user_entry = next(user_list.filter(f'name="{username}"'))
            return user_entry.ordering_index
        except StopIteration:
            self.log.debug(f'User {username} not found in {user_list._path}')
            return None

    @retry_on_conflict()
    def _remove_old_management_user(self, device, new_credentials):
        """Cleanup old management credentials

        1. If the user was consumed during the initial credentials discovery
           stage, store the user in the current-management-user leaf.

        2. When creating the final management credentials user-account service,
           make sure the new user list entry is owned by the user-account
           service. This is done by using the
           ncs.maapi.COMMIT_NCS_RECONCILE_DISCARD_NON_SERVICE_CONFIG commit
           flag. Note: empirical evidence suggests this is not sufficient to
           reconcile an existing user - after the operation refcount is still 2.
           To that end, a second re-deploy reconcile is made after creating the
           instance. This still works out correctly if there are other service
           instances owning the same config.

        3. When changing (to) the final management credentials, remove the user
           listed in the current-management-user leaf. If the user-account
           service instance exists for the old user, remove it. If the instance
           does not exist, remove the user list entry manually by editing the
           config.
        """
        with self.maapi.start_read_trans() as t_read:
            root = ncs.maagic.get_root(t_read)
            device_automaton = root.ncs__devices.devaut__automaton[device]
            old_management_user = device_automaton.current_management_user
            user_service_exists = old_management_user in root.devices.device[device].devaut__user_account

        if old_management_user and old_management_user != new_credentials.username:
            self.log.debug(f'remove-old-management-user: {old_management_user} -> {new_credentials.username}')
            with self.maapi.start_write_trans() as t_write:
                root = ncs.maagic.get_root(t_write)
                if user_service_exists:
                    self.log.debug('remove-old-management-user: deleting old user-account service')
                    del root.devices.device[device].devaut__user_account[old_management_user]
                else:
                    self.log.debug('remove-old-management-user: deleting user manually')
                    ncs_device = root.devices.device[device]
                    dc = classes.DeviceCapability(ncs_device)
                    c = ncs_device.config
                    if dc.check_capability('http://xml.juniper.net/xnm/1.1/xnm'):
                        self._safe_delete(c.junos__configuration.system.login.user, old_management_user)
                    elif dc.check_capability('http://cisco.com/ns/yang/Cisco-IOS-XR-aaa-locald-admin-cfg'):
                        self._safe_delete(c.aaa_locald_admin_cfg__aaa.usernames.username, old_management_user)
                    elif dc.check_capability('http://cisco.com/ns/yang/Cisco-IOS-XR-aaa-lib-cfg'):
                        ordering_index = self._find_user_ordering_index(c.aaa_lib_cfg__aaa.usernames.username, old_management_user)
                        if ordering_index is not None:
                            self._safe_delete(c.aaa_lib_cfg__aaa.usernames.username, ordering_index, old_management_user)
                    elif dc.check_capability('http://cisco.com/ns/yang/Cisco-IOS-XR-um-aaa-cfg'):
                        if hasattr(c, 'Cisco_IOS_XR_um_aaa_cfg__aaa'):
                            prefix = 'Cisco_IOS_XR_um_aaa_cfg'
                        else:
                            prefix = 'um_aaa_cfg'
                        aaa = getattr(c, f'{prefix}__aaa')
                        ordering_index = self._find_user_ordering_index(aaa.usernames.username, old_management_user)
                        if ordering_index is not None:
                            self._safe_delete(aaa.usernames.username, ordering_index, old_management_user)
                    elif dc.check_capability('http://openconfig.net/yang/system'):
                        self._safe_delete(c.oc_sys__system.aaa.authentication.users.user, old_management_user)
                    elif dc.check_capability('http://tail-f.com/ned/huawei-vrp'):
                        self._safe_delete(c.vrp__rsa.peer_public_key, old_management_user)
                        self._safe_delete(c.vrp__aaa.local_user, old_management_user)
                        self._safe_delete(c.vrp__ssh.user, old_management_user)
                    elif dc.check_capability('http://tail-f.com/ned/alu-sr'):
                        self._safe_delete(c.alu__system.security.user, old_management_user)
                    elif dc.check_capability('urn:nokia.com:sros:ns:yang:sr:conf'):
                        self._safe_delete(c.conf__configure.system.security.user_params.local_user.user, old_management_user)
                    elif dc.check_capability('urn:ios'):
                        self._safe_delete(c.username, old_management_user)
                    else:
                        self.log.error(f'Unhandled set of capabilities on device {device}')

                root.ncs__devices.devaut__automaton[device].current_management_user = new_credentials.username
                self._take_timeout(device, t_write)
                # On older versions of IOS XR (6.2.x) removing the user via NETCONF may fail
                try:
                    # Skip check-sync (for this commit only) because the initial user (vrnetlab for instance)
                    # has some additional associated configuration in other places that gets automagically
                    # removed when the user is removed.
                    t_write.apply(flags=ncs.maapi.COMMIT_NCS_BYPASS_COMMIT_QUEUE | ncs.maapi.COMMIT_NCS_NO_OUT_OF_SYNC_CHECK)
                except Exception as e:
                    if "'YANG framework' detected the 'fatal' condition" in str(e):
                        self.log.error(f'remove-old-management-user: deleting user {old_management_user} failed on IOS-XR')
                    else:
                        raise

    @retry_on_conflict()
    def _create_device_authgroup(self, name: str, credentials: DeviceCredentials):
        """Create or update the NSO authgroup with given credentials"""
        tvars = utils.prepare_template_variables(credentials.asdict())
        tvars.add('NAME', name)
        with self.maapi.start_write_trans() as t_write:
            try:
                root = ncs.maagic.get_root(t_write)
                ncs.template.Template(root.devices).apply(TemplateName('devaut-authgroup-service'), tvars)
                # Force re-deployment in case the /devices/authgroups/group entry got changed out
                # from under us, but we're trying to use the currently configured credentials.
                root.devices.authgroups.devaut__authgroup[name].touch()
                t_write.apply()
            except _ncs.error.Error as e:
                # Checking only for certain case which is if RSA_PRIVATE_KEY is of wrong format
                # not sure what the code is so added string check also
                if e.confd_errno == 19 and "RSA_PRIVATE_KEY" in str(e):
                    raise Exception('RSA key has invalid value on path: {}/private-key'.format(credentials.path)) from e
                raise


def get_device_credentials(root: ncs.maagic.Root, device: str) -> Optional[DeviceCredentials]:
    """Read the currently configured device credentials for the given device by looking at the authgroup

    If the device-specific authgroup exists, the returned credentials are decrypted and the private
    RSA key, if present, prepared in the expected format.

    :param root: maagic Root
    :param device: device name
    :returns: populated DeviceCredentials object if authgroup exists, or None"""

    authgroup_name = root.devices.device[device].authgroup
    try:
        authgroup = root.devices.authgroups.group[authgroup_name].default_map
    except KeyError:
        return None
    username = authgroup.remote_name
    encrypted_password = authgroup.remote_password
    if encrypted_password is not None:
        password = utils.decrypt_encrypted_string(root, encrypted_password)
    else:
        password = None

    private_key_name = authgroup.public_key.private_key.name
    if private_key_name is not None:
        encrypted_private_key = root.ssh.private_key[private_key_name].key_data
        pk_erlang_term_binary = utils.decrypt_encrypted_string(root, encrypted_private_key).encode('utf-8')
        # The private key stored in /ssh/private-key/key-data is a
        # base64-encoded erlang term with the following structure for an RSA key:
        #  (OtpErlangAtom(b'RSAPrivateKey),
        #   OtpErlangBinary(b<RSA key in binary),
        #   OtpErlangAtom(b'not_encrypted))
        # For other supported types (ECDSA, ED25519) the first element is a tuple:
        #  (OtpErlangAtom(b'no_asn1'), OtpErlangAtom(b'new_openssh'))
        # and the rest is the same.
        # We reconstruct the PEM encoded key here by re-encoding the binary key
        # (second element in the term) with base64 and add appropriate label in the wrapper.

        pk_erlang_term = erlang.binary_to_term(base64.b64decode(pk_erlang_term_binary))
        if isinstance(pk_erlang_term[0], erlang.OtpErlangAtom) and pk_erlang_term[0] == erlang.OtpErlangAtom(b'RSAPrivateKey'):
            pk_label = 'RSA'
            pk_algorithm = SshKeyAlgorithm.SSH_RSA
        elif b'ecdsa' in pk_erlang_term[1].value:
            pk_label = 'OPENSSH'
            pk_algorithm = SshKeyAlgorithm.ECDSA_SHA2_NISTP256
        elif b'ed25519' in pk_erlang_term[1].value:
            pk_label = 'OPENSSH'
            pk_algorithm = SshKeyAlgorithm.SSH_ED25519
        else:
            raise ValueError('Unsupported private key type')
        pk_b64 = base64.b64encode(pk_erlang_term[1].value).decode('utf-8)')
        # Now split the b64encoded string to lines of 64 characters and add the wrapper
        private_key = f'-----BEGIN {pk_label} PRIVATE KEY-----\n' + \
                      '\n'.join(pk_b64[i:i + 64] for i in range(0, len(pk_b64), 64)) + \
                      f'\n-----END {pk_label} PRIVATE KEY-----\n'
    else:
        private_key = None
        pk_algorithm = None

    dc = DeviceCredentials(username, password=password, algorithm=pk_algorithm, private_key=private_key)
    return dc


def _main():
    import logging
    logging.basicConfig(level=logging.DEBUG)
    with ncs.maapi.single_read_trans('', 'system') as t:
        read_root = ncs.maagic.get_root(t)
        m = ncs.maagic.get_maapi(t)
        djc = DeviceCredentialJuggler(read_root, m, logging)
        djc.setup_initial_device_credentials(read_root.ncs__devices.devaut__automaton['dut-r1'])


if __name__ == '__main__':
    _main()
