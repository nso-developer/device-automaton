Learn by Doing: Device Automaton
================================

This directory contains Makefile recipes that set up the Device Automaton
package in your Cisco NSO reservable sandbox. The purpose is to give you quick
access to a working environment with the automaton package loaded so that you
can easily observe its behavior. The NSO environment is a local installation of
NSO on the "DevBox" machine in the sandbox (10.10.20.50).

In addition to the NEDs for the devices used in the lab there are two packages copied from this repository:
1. `device-automaton` itself,
2. `cpe-example` containing a simple node (nano-)service, using the automaton to
   create a device and another service to apply network configuration,
   orchestrated by a "node" service.

This example assumes a working knowledge of NSO services and optionally nano-services.

Installation
------------

Reserve the [Cisco NSO reservable sandbox].

If you need to revisit some of the NSO Basics, you can [start here].

[Cisco NSO reservable sandbox]: https://devnetsandbox.cisco.com/RM/Diagram/Index/43964e62-a13c-4929-bde7-a2f68ad6b27c?diagramType=Topology
[start here]: https://developer.cisco.com/learning/lab/learn-nso-the-easy-way/step/1

Clone [this repository] (device-automaton) on the DevBox machine in the sandbox.
To access the machine you can use either SSH or RDP to 10.10.20.50 with
credentials developer/C1sco12345.

```
(py3venv) [developer@devbox ~]$ git clone https://gitlab.com/nso-developer/device-automaton.git
Cloning into 'device-automaton'...
remote: Enumerating objects: 2845, done.
remote: Counting objects: 100% (163/163), done.
remote: Compressing objects: 100% (96/96), done.
remote: Total 2845 (delta 71), reused 125 (delta 53), pack-reused 2682
Receiving objects: 100% (2845/2845), 617.14 KiB | 0 bytes/s, done.
Resolving deltas: 100% (1515/1515), done.
```

The recipe for setting up a local installation of NSO and the required packages
is placed in the `devnet-sandbox` directory. Change the directory and run the
`magic` recipe. After a successful run the local instance of NSO is up and
running with the automaton package loaded.

```
(py3venv) [developer@devbox ~]$ cd device-automaton/devnet-sandbox/
(py3venv) [developer@devbox devnet-sandbox]$ make magic
[... a lot of output ...]
make[1]: Entering directory `/home/developer/device-automaton/devnet-sandbox'
ncs --cd ../tmp/nso-devaut
make[1]: Leaving directory `/home/developer/device-automaton/devnet-sandbox'
```

[this repository]: https://gitlab.com/nso-developer/device-automaton.git

Create an automaton service instance
------------------------------------

The local NSO instance starts up empty. While all the required packages are
loaded, no devices are added. Next you will add one of the devices from the
sandbox. The list below contains all data needed by the automaton:

- device name: **dist-rtr01**
- OS: Cisco IOS XE (CLI)
- management address: **10.10.20.175**
- management credentials: **cisco/cisco**

Let's map these parameters to the device automaton service model:

- device name: `device` (key leaf)
- OS: `ned-id`
- management address: entry in the `management-endpoint` list (key leaf)
- management-credentials: `username` and `password` leaf in the
  `management-credentials` container`

You can now create the automaton service instance using the following configuration:

```
devices {
    automaton dist-rtr01 {
        ned-id cisco-ios-cli-6.67;
        management-endpoint 10.10.20.175;
        management-credentials {
            username cisco;
            password cisco;
        }
    }
}
```

Tip: you can paste this configuration directly into NSO CLI (configure mode) by
first using the command `load merge terminal`, pasting and then pressing Ctrl+D

Observe the output of `commit dry-run` for the service instance. Note that it
contains a `/devices/device` list entry - this is the new device that will be
created after you commit the change. The device is also associated with an
unique authgroup to ensure different credentials can be configured for different
devices.

```
admin@ncs% commit dry-run
cli {
    local-node {
        data  devices {
                  authgroups {
             +        group dist-rtr01 {
             +            default-map {
             +                remote-name cisco;
             +                remote-password $9$GNpSlwl68IwYCcvElCPzGxbGQ9rmwCugNslg9N2yXFc=;
             +            }
             +        }
                  }
             +    device dist-rtr01 {
             +        address 10.10.20.175;
             +        authgroup dist-rtr01;
             +        device-type {
             +            cli {
             +                ned-id cisco-ios-cli-6.67;
             +            }
             +        }
             +        commit-queue {
             +            enabled-by-default false;
             +        }
             +        state {
             +            admin-state unlocked;
             +        }
             +    }
             +    automaton dist-rtr01 {
             +        ned-id cisco-ios-cli-6.67;
             +        management-endpoint 10.10.20.175;
             +        management-credentials {
             +            username cisco;
             +            password $9$GNpSlwl68IwYCcvElCPzGxbGQ9rmwCugNslg9N2yXFc=;
             +        }
             +    }
              }
    }
}
```

Immediately after you commit the service instance the automaton creates the
device list entry. But this device is not yet ready to accept configuration. The
automaton must then execute the necessary actions that get NSO fully synced up
with the current device configuration.

Most of these interesting tasks done by the automaton are done *asynchronously*,
after you commit the service instance. These tasks are visible to users (and
other services) through operational data available on the service instance. The
operational data model of the automaton contains a *plan* where the main
*component* has multiple *states* with tracked *statuses*. If these terms sound
familiar it is because they are the same as used by (the now obsolete reactive
fastmap services and) nano services. The automaton is not really a
nano-service - the states do not really emit any configuration but rather
perform actions (side-effects). Nevertheless, the user-facing interface is
intentionally kept similar because most users will have heard of nano services
and know that the presence of a *plan* implies the service is not "done"
immediately after commit.

The device automaton plan for a completed service is shown below. Note that all *states* have their *status* set to reached.

```
admin@ncs> show devices automaton plan
                             LOG
DEVICE      FAILED  MESSAGE  ENTRY  NAME        TYPE    STATE                              STATUS   WHEN                 ref  ID
----------------------------------------------------------------------------------------------------------------------------------
dist-rtr01  -       -        -      self        self    init                               reached  2021-08-12T21:46:48  -
                                                        ready                              reached  2021-08-12T21:46:58  -
                                    dist-rtr01  device  init                               reached  2021-08-12T21:46:48  -
                                                        management-ip-reachable            reached  2021-08-12T21:46:49  -
                                                        ssh-host-key-fetched               reached  2021-08-12T21:46:50  -
                                                        initial-credentials-configured     reached  2021-08-12T21:46:51  -
                                                        device-type-detected               reached  2021-08-12T21:46:51  -
                                                        commit-queue-handled               reached  2021-08-12T21:46:51  -
                                                        sync-from-performed                reached  2021-08-12T21:46:54  -
                                                        tricks-performed                   reached  2021-08-12T21:46:54  -
                                                        management-credentials-configured  reached  2021-08-12T21:46:58  -
                                                        ready                              reached  2021-08-12T21:46:58  -
```

If any of these steps fail the associated *state* is set to failed *status*. The
process is then automatically restarted after a timeout, or it can be manually
triggered by executing the `enqueue-work` action on the (failed) service
instance.
