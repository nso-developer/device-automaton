#!/usr/bin/env python
import telnetlib
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('host')
args = parser.parse_args()

tn = telnetlib.Telnet(args.host)
while True:
    idx, match, data = tn.expect([b'Username.*', b'Password.*', b'.*#$'], 1)
    print('%s %s' % (idx, data))
    if idx == 0:
        tn.write(b"cisco\n")
    elif idx == 1:
        tn.write(b"cisco\n")
    elif idx == 2:
        break
tn.write(b"terminal length 0\n")
tn.write(b"show version\n")
ver = tn.read_until(b'#').decode('ascii')
if 'IOS XRv' in ver:
    tn.write(b"crypto key generate rsa\n")
else:
    tn.write(b"crypto key generate rsa modulus 4096\n")
print(tn.read_until(b'#'))
tn.write(b"exit\n")
