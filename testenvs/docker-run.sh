#!/bin/bash
# Wrapper that makes 'docker run ...' idempotent. If the container does not
# exist it will create and run a new container. If the container exists but is
# stopped it will remove the container and then restart a new one. If the
# container is running this is a NOOP.
#
# Usage:
# docker-run.sh --name <container-name> <other docker run parameters>
#

# do basic argument parsing and grab name of container
pointer=1
DOCKER_PARAMS=""
while [[ $pointer -le $# ]]; do
    param=${!pointer}
    case $param in
        --name=*)
            name="${param#*=}"
            DOCKER_PARAMS="$DOCKER_PARAMS $param"
            ;;
        --name)
            ((pointer++))
            name=${!pointer}
            DOCKER_PARAMS="$DOCKER_PARAMS $param $name"
            ;;
        --network-alias=*)
            network_alias="${param$*=}"
            DOCKER_PARAMS="$DOCKER_PARAMS $param"
            ;;
        --network-alias)
            ((pointer++))
            network_alias=${!pointer}
            DOCKER_PARAMS="$DOCKER_PARAMS $param $network_alias"
            ;;
        *)
            DOCKER_PARAMS="$DOCKER_PARAMS $param"
            ;;
    esac
    ((pointer++))
done
if [ -z "${name}" ]; then
    echo "ERROR: no container name specified"
    exit 1
fi
CNT=$name
# does container exist?
! docker inspect --format '{{.State.Running}}' ${CNT} >/dev/null 2>&1
EXIST=$?
# is container running?
! docker inspect --format '{{.State.Running}}' ${CNT} 2>/dev/null | grep true >/dev/null
RUNNING=$?

# If --network-alias was provided then also use it to set the hostname.
# We could also use the container name but that would require translating some
# characters that are not valid for a hostname.
if [ ! -z "${network_alias}" ]; then
    EXTRA_FLAGS="--hostname ${network_alias}"
fi

if [ ${EXIST} -eq 0 ]; then
    echo "Container $CNT doesn't exist - starting"
elif [ ${RUNNING} -eq 0 ]; then
    echo "Container $CNT exists but is not running - removing and starting"
    docker rm -f ${CNT}
else
    echo "Container $CNT exists and is running - NOOP"
    exit 0
fi

set -e
eval docker run $EXTRA_FLAGS $DOCKER_PARAMS
