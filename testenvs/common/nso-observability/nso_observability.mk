
COMMON_DIR=$(dir $(realpath $(lastword $(MAKEFILE_LIST))))

# By default we run the observability stack in local testenv, but not in CI.
ifdef ($(CI))
NSO_OBSERVABILITY?=false
else
NSO_OBSERVABILITY?=true
endif

nsobs-start: DOCKER_LABEL_ARG+= --label com.cisco.nso.testenv.nsobs=$(CNT_PREFIX)
nsobs-start:
ifeq ($(NSO_OBSERVABILITY),true)
	@echo "== Starting NSO Observability stack"

	@echo "-- Starting Tempo"
	docker create -tP --name $(CNT_PREFIX)-tempo --network-alias tempo $(DOCKER_ARGS) grafana/tempo:1.5.0 -config.file=/tempo.yaml
	docker cp $(COMMON_DIR)/tempo.yaml $(CNT_PREFIX)-tempo:/tempo.yaml
	docker start $(CNT_PREFIX)-tempo

	@echo "-- Starting OpenTelemetry Collector"
	docker create -tP --name $(CNT_PREFIX)-otelcol --network-alias otelcol $(DOCKER_ARGS) otel/opentelemetry-collector --config /otelcol.yaml
	docker cp $(COMMON_DIR)/otelcol.yaml $(CNT_PREFIX)-otelcol:/otelcol.yaml
	docker start $(CNT_PREFIX)-otelcol

	@echo "-- Starting elasticsearch"
	docker run -tdP --name $(CNT_PREFIX)-elasticsearch --network-alias elasticsearch $(DOCKER_ARGS) \
		-e "discovery.type=single-node" elasticsearch:7.8.0

	@echo "-- Starting Grafana"
	docker run -tdP --name $(CNT_PREFIX)-grafana --network-alias grafana $(DOCKER_ARGS) \
    -e GF_AUTH_ANONYMOUS_ENABLED=true \
    -e GF_AUTH_ANONYMOUS_ORG_NAME=NSO \
    -e GF_AUTH_ANONYMOUS_ORG_ROLE=Admin \
    -e GF_AUTH_DISABLE_LOGIN_FORM=true \
    -e GF_AUTH_DISABLE_SIGNOUT_MENU=true \
    -e GF_DASHBOARDS_JSON_ENABLED=true \
		grafana/grafana

	@echo "-- Starting Jaeger collector"
	docker run -tdP --name $(CNT_PREFIX)-jaeger-collector --network-alias jaeger-collector $(DOCKER_ARGS) \
		-e SPAN_STORAGE_TYPE=elasticsearch -e ES_SERVER_URLS=http://elasticsearch:9200/ --restart=always jaegertracing/jaeger-collector:1.18.1

	@echo "-- Starting Jaeger query"
	docker run -tdP --name $(CNT_PREFIX)-jaeger --network-alias jaeger $(DOCKER_ARGS) \
		-e SPAN_STORAGE_TYPE=elasticsearch -e ES_SERVER_URLS=http://elasticsearch:9200/ --restart=always jaegertracing/jaeger-query:1.18.1
	docker run -tdP --name $(CNT_PREFIX)-node-exporter --network-alias node-exporter $(DOCKER_ARGS) prom/node-exporter

	@echo "-- Starting InfluxDB"
	docker run -tdP --name $(CNT_PREFIX)-influxdb --network-alias influxdb $(DOCKER_ARGS) influxdb:1.8

	@echo "-- Start jaeger agent - allowed to fail in case the NSO container is not started yet"
	-$(MAKE) nsobs-start-jaeger-agent
endif


nsobs-stop:
	docker ps -aq --filter label=com.cisco.nso.testenv.nsobs=$(CNT_PREFIX) | $(XARGS) docker rm -vf

nsobs-start-jaeger-agent: DOCKER_LABEL_ARG+= --label com.cisco.nso.testenv.nsobs=$(CNT_PREFIX)
nsobs-start-jaeger-agent:
	@echo "-- Starting Jaeger agent as side car to $(CNT_PREFIX)-nso$(NSO)"
	docker run -td --name $(CNT_PREFIX)-jaeger-agent --network=container:$(CNT_PREFIX)-nso$(NSO) $(DOCKER_LABEL_ARG) \
	  jaegertracing/jaeger-agent:1.18.1 --reporter.grpc.host-port=jaeger-collector:14250

nsobs-config-nso:
ifeq ($(NSO_OBSERVABILITY),true)
	@echo "-- Configuring Observability export of data in NSO"
	$(MAKE) runcmdJ CMD="configure\nedit progress export\n set enabled\ncommit"
	$(MAKE) runcmdJ CMD="configure\nedit progress trace ptrace\n set enabled verbosity very-verbose destination file ptrace.csv format csv\ncommit"
	$(MAKE) runcmdJ CMD="configure\nset python-vm logging vm-levels observability-exporter level level-debug\ncommit"
	$(MAKE) runcmdJ CMD="configure\nedit progress export\n set enabled\nset otlp host otelcol\nset influxdb host influxdb\nset jaeger-base-url http://$(shell docker port $(CNT_PREFIX)-jaeger 16686/tcp $(DPR))\ncommit\nexit\nexit\nrequest progress export restart"
endif

nsobs-prepare:
ifeq ($(NSO_OBSERVABILITY),true)
	$(MAKE) nsobs-prepare-grafana
endif

# Rewrite of the docker port output...
DPR=| sed -e 's/0.0.0.0/127.0.0.1/g' -e 's/^:::/[::1]:/' -ne '1p'
nsobs-prepare-grafana: dashboard-nso.json
	@echo "-- Configure Grafana org name"
	curl "http://admin:admin@$(shell docker port $(CNT_PREFIX)-grafana 3000/tcp $(DPR))/api/orgs/1" \
		-m 5 -X PUT --noproxy '*' \
		-H 'Content-Type: application/json;charset=UTF-8' \
		--data-binary "{\"name\":\"NSO\"}"; echo
	@echo "-- Add Grafana data source for ElasticSearch"
	curl "http://admin:admin@$(shell docker port $(CNT_PREFIX)-grafana 3000/tcp $(DPR))/api/datasources" \
			-m 5 -X POST --noproxy '*' \
			-H 'Content-Type: application/json;charset=UTF-8' \
			--data-binary '{"id":1,"orgId":1,"name":"Elasticsearch","type":"elasticsearch","typeLogoUrl":"public/app/plugins/datasource/elasticsearch/img/elasticsearch.svg","access":"proxy","url":"http://elasticsearch:9200","password":"","user":"","database":"","basicAuth":false,"isDefault":false,"jsonData":{"esVersion":70,"logLevelField":"","logMessageField":"","maxConcurrentShardRequests":5,"timeField":"@timestamp"},"readOnly":false}'; echo
	@echo "-- Add Grafana data source for InfluxDB"
	curl "http://admin:admin@$(shell docker port $(CNT_PREFIX)-grafana 3000/tcp $(DPR))/api/datasources" \
			-m 5 -X POST --noproxy '*' \
			-H 'Content-Type: application/json;charset=UTF-8' \
			--data-binary '{"id":2,"orgId":1,"name":"InfluxDB","type":"influxdb","typeLogoUrl":"public/app/plugins/datasource/influxdb/img/influxdb_logo.svg","access":"proxy","url":"http://influxdb:8086","password":"","user":"","database":"nso","basicAuth":false,"isDefault":false,"jsonData":{"esVersion":70,"logLevelField":"","logMessageField":"","maxConcurrentShardRequests":5,"timeField":"@timestamp"},"readOnly":false}'; echo
	@echo "-- Add Grafana dashboard for node-exporter"
	@curl "http://admin:admin@$(shell docker port $(CNT_PREFIX)-grafana 3000/tcp $(DPR))/api/dashboards/db/node" \
			-m 5 -X DELETE --noproxy '*' \
			-H 'Content-Type: application/json;charset=UTF-8'; echo
	@curl "http://admin:admin@$(shell docker port $(CNT_PREFIX)-grafana 3000/tcp $(DPR))/api/dashboards/db" \
			-m 5 -X POST --noproxy '*' \
			-H 'Content-Type: application/json;charset=UTF-8' \
			--data-binary @dashboard-node.json; echo
	@echo "-- Add Grafana dashboard for NSO"
	$(MAKE) import-dashboard
	@echo "-- Set NSO dashboard as default dashboard"
	@curl 'http://admin:admin@$(shell docker port $(CNT_PREFIX)-grafana 3000/tcp $(DPR))/api/org/preferences' \
			-m 5 -X PUT --noproxy '*' \
			-H 'X-Grafana-Org-Id: 1' \
			-H 'Content-Type: application/json;charset=UTF-8' \
			--data-binary "{\"homeDashboardId\":$$(curl -m 5 --noproxy '*' 'http://admin:admin@$(shell docker port $(CNT_PREFIX)-grafana 3000/tcp $(DPR))/api/dashboards/uid/nso' 2>/dev/null | jq .dashboard.id)}"; echo

.PHONY: import-dashboard save-dashboard
save-dashboard:
	@curl "http://admin:admin@$(shell docker port $(CNT_PREFIX)-grafana 3000/tcp $(DPR))/api/dashboards/uid/nso" \
			-m 5 -X GET --silent --noproxy '*' \
			-H 'Content-Type: application/json;charset=UTF-8' | jq '.dashboard | .id=null ' \
				| jq '.templating.list[].current.text="$${INPUT_JAEGER_BASE_URL}"' \
				| jq '.templating.list[].current.value="$${INPUT_JAEGER_BASE_URL}"' \
				| jq '.templating.list[].query="$${INPUT_JAEGER_BASE_URL}"' \
				| sed -e 's/"datasource": "InfluxDB",/"datasource": "$${DS_INFLUXDB}",/' \
				| jq --indent 4 . | tail -n +2 | cat $(COMMON_DIR)/props/dashboard-nso-header /dev/stdin > $(COMMON_DIR)/props/dashboard-nso.json.in

.PHONY: dashboard-nso.json
dashboard-nso.json:
	@if [ -f "$(COMMON_DIR)props/dashboard-nso.json.in" ]; then \
		cp $(COMMON_DIR)/props/dashboard-nso.json.in $@; \
	else \
		curl "http://admin:admin@$(shell docker port $(CNT_PREFIX)-grafana 3000/tcp $(DPR))/api/gnet/dashboards/14353" \
			-m 10 -X GET --noproxy '*' \
				| jq '.json' > $@; \
	fi

dashboard-req.json: dashboard-nso.json
	jq '{"dashboard": . , "overwrite": true, "inputs":[{"name":"DS_INFLUXDB","type":"datasource", "pluginId":"influxdb","value":"InfluxDB"},{"name":"INPUT_JAEGER_BASE_URL","type":"constant","value":"http://$(shell docker port $(CNT_PREFIX)-jaeger 16686/tcp $(DPR))/"}]}' dashboard-nso.json > $@

import-dashboard: dashboard-req.json
	@curl "http://admin:admin@$(shell docker port $(CNT_PREFIX)-grafana 3000/tcp $(DPR))/api/dashboards/import" \
			-m 5 -X POST -H "Accept: application/json" --noproxy '*' \
			-H 'Content-Type: application/json;charset=UTF-8' --data @dashboard-req.json; echo

nsobs-print-ui-addresses:
ifeq ($(NSO_OBSERVABILITY),true)
	@echo "Visit the following URLs in your web browser to reach respective system:"
	@echo "Jaeger    : http://$(shell docker port $(CNT_PREFIX)-jaeger 16686/tcp $(DPR))"
	@echo "Grafana   : http://$(shell docker port $(CNT_PREFIX)-grafana 3000/tcp $(DPR))"
	@echo "InfluxDB  : http://$(shell docker port $(CNT_PREFIX)-influxdb 8086/tcp $(DPR))"
endif


DASHBOARDID=$(shell curl -m 5 --noproxy '*' 'http://admin:admin@$(shell docker port $(CNT_PREFIX)-grafana 3000/tcp $(DPR))/api/dashboards/uid/nso' 2>/dev/null | jq .dashboard.id)
nsobs-add-annotation:
	@curl 'http://admin:admin@$(shell docker port $(CNT_PREFIX)-grafana 3000/tcp $(DPR))/api/annotations' \
			-m 5 -X POST --noproxy '*' \
			-H 'Content-Type: application/json;charset=UTF-8' \
			-d '{"dashboardId": $(DASHBOARDID), "text":"Start of $(TESTID)-$()"}'

nsobs-get-annotations:
	@curl 'http://admin:admin@$(shell docker port $(CNT_PREFIX)-grafana 3000/tcp $(DPR))/api/annotations?from=1618836000000' \
			-m 5 -X GET --noproxy '*' \
			-H 'X-Grafana-Org-Id: 1' \
			-H 'Content-Type: application/json;charset=UTF-8' | jq


nsobs-wipe-annotations:
	@curl 'http://admin:admin@$(shell docker port $(CNT_PREFIX)-grafana 3000/tcp $(DPR))/api/annotations?from=1618836000000' \
			-m 5 -X GET --silent --noproxy '*' \
			-H 'X-Grafana-Org-Id: 1' \
			-H 'Content-Type: application/json;charset=UTF-8' | jq '.[].id' \
			| xargs -n1 -IXXX \
				curl --noproxy '*' 'http://admin:admin@$(shell docker port $(CNT_PREFIX)-grafana 3000/tcp $(DPR))/api/annotations/XXX' \
			-m 5 -X DELETE \
			-H 'X-Grafana-Org-Id: 1' \
			-H 'Content-Type: application/json;charset=UTF-8'



START_MARK_ID=$(shell curl --noproxy '*' 'http://admin:admin@$(shell docker port $(CNT_PREFIX)-grafana 3000/tcp $(DPR))/api/annotations?tags=mark-$(TESTID)-$*' \
			-m 5 -X GET --silent \
			-H 'Content-Type: application/json;charset=UTF-8' | jq .[].id)

# Mark Start and Mark End
_mark_start/%:
	@curl 'http://admin:admin@$(shell docker port $(CNT_PREFIX)-grafana 3000/tcp $(DPR))/api/annotations' \
			-m 5 -X POST --silent --noproxy '*' \
			-H 'Content-Type: application/json;charset=UTF-8' \
			-d '{"text":"Running test $*", "tags":["testmarker", "mark-$(TESTID)-$*"]}' >/dev/null

START_MARK_ID=$(shell curl --noproxy '*' 'http://admin:admin@$(shell docker port $(CNT_PREFIX)-grafana 3000/tcp $(DPR))/api/annotations?tags=mark-$(TESTID)-$*' \
			-m 5 -X GET --silent \
			-H 'Content-Type: application/json;charset=UTF-8' | jq .[].id)

_mark_end/%:
	@curl 'http://admin:admin@$(shell docker port $(CNT_PREFIX)-grafana 3000/tcp $(DPR))/api/annotations/$(START_MARK_ID)' \
			-m 5 -X PATCH --silent --noproxy '*' \
			-H 'Content-Type: application/json;charset=UTF-8' \
			-d '{"timeEnd": $(shell date +%s%3N)}' >/dev/null

# Mark Start and Mark End
MS=$(MAKE) --no-print-directory _mark_start/$@
ME=$(MAKE) --no-print-directory _mark_end/$@
